import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { Post } from '../model/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getAllPost(): Observable<Post[]> {
    return this.http.get<Post[]>(`https://localhost:44368/api/students`).pipe(map(
      result => {
        return result.map(p => new Post(p.id, p.fullName, p.yearofBirth));
      }
    ));
  }

  updatePost(id: number, data: Post): Observable<any> {
    return this.http.put<any>(`https://localhost:44368/api/students/${id}`, { ...data });
  }
}

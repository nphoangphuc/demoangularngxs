import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Post } from './model/post.model';
import { GetPosts } from './ngxs/post.action';
import { PostState } from './ngxs/post.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ngxs-demo';

  @Select(PostState.getAllPost) 
  post$: Observable<Post[]>

  constructor(private store: Store) {
      this.store.dispatch(GetPosts);
  }

}

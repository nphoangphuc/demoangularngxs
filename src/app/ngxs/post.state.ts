import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { PostStore } from './post.store';
import { PostService } from '../service/post.service';
import { GetPosts, UpdatePost } from './post.action';
import { tap } from 'rxjs/operators'

@State<PostStore>({
  name: 'PostStore',
  defaults: {
    posts: []
  }
})

@Injectable()
export class PostState {
    constructor(private postService: PostService) {}

    @Selector()
    static getAllPost(state: PostStore) {
      return state.posts;
    }

    @Action(GetPosts)
      getPosts(ctx: StateContext<PostStore>) {
        return this.postService.getAllPost().pipe(tap(
          result => {
            const state = ctx.getState();
            ctx.setState({
              ...state,
              posts: result
            });
          }
        ))
      }

      @Action(UpdatePost)
      updatePost(ctx: StateContext<PostStore>, { id, payload}: UpdatePost) {
        return this.postService.updatePost(id, payload).pipe(tap(
          result => {
            const state = ctx.getState();
            
            let listPost = state.posts;

            let index = listPost.findIndex(p => p.id === id);
            
            listPost[index] = payload;

            ctx.setState({
              ...state,
              posts: listPost
            });
          }
        ))
      }
}